var maxEnemies = 5;
var numEnemies = 0;

var score = 0;

var maxBullets = 5;
var numBullets = 0;

// Removes and enemy
// o    ::  Enemy to be removed
function killEnemy(o)
{
    o.destroy();
    numEnemies--;   
}

// Adds a new enemy
function addEnemy()
{
    var x = Math.random() * 400 + 16;
    var y = -20;
    Crafty.e('Enemy1').at(x, y);
    numEnemies++;
}

function setScore(score)
{
    this.score = score;
}



Game = 
{

    width: function()
    {
        return 400;
    },
    
    height: function()
    {
        return 540;
    },
    
    // Initialize and start our game
    start: function()
    {
        // start crafty and set a background colour
        // FPS defaults to 60
        Crafty.init(Game.width(), Game.height());
        //Crafty.background('green');
        Crafty.scene('Loading');
    },
    
    
}

$text_css = { 'font-size': '24px', 'font-family': 'Arial', 'color': 'white', 'text-align': 'center' }
