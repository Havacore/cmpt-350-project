

Crafty.scene('Game', function(){
    
    Crafty.audio.play('redDoors', -1, 0.5);
    
    this.maxEnemies = 10;
    this.numEnemies = 0;
    this.spawnChance = 0.05;
    this.player;
    
    
    // initialize the player and one enemy
    this.player = Crafty.e('PC');
    
    this.scoreBoard = Crafty.e('ScoreBoard').css($text_css);
    this.leftWall = Crafty.e('Wall_Left');
    this.rightWall = Crafty.e('Wall_Right');
    this.topWall = Crafty.e('Wall_Top');
    this.bottomWall = Crafty.e('Wall_Bottom');
    
    
    
    
    
    // Main loop
    this.bind("EnterFrame", function(e)
    {
        if( numEnemies <= maxEnemies &&
            Math.random() < this.spawnChance)
        {
            addEnemy();
        }
    });
    
    this.bind('KeyDown', function(e)
    {
       if(e.key == Crafty.keys['ENTER']) Crafty.scene('Pause'); 
    });
    
});// ------------------------------------------------------------------------//

Crafty.scene('GameOver', function(){
   
   Crafty.e('GameOver_Screen').css($text_css);
   Crafty('GameOver_Screen').setScore(score);
   
}); // ------------------------------------------------------------------------//

Crafty.scene('Loading', function(){
    

    
    this.loading = Crafty.e('LoadingScreen').css($text_css);
    
    // Set the background to the space image
    Crafty.load('assets/space-2.png', function(){})
    Crafty.background('url(assets/space-2.png)');
    
    // load our spaceships
    Crafty.load(['assets/space-2.png', 'assets/two_ships.png', 'assets/boom3_1.png','assets/redDoors.mp3', 
    'assets/explosion.wav'], function(){
        
        Crafty.sprite(64, 'assets/two_ships.png', {
        spr_pc:     [0,0],
        spr_enemy:  [0,1]
        });
        
        Crafty.sprite(128, 'assets/boom3_1.png', {
            spr_explosion: [0,0]
        });
        
        Crafty.audio.add('redDoors', 'assets/Red_Doors.mp3');
        Crafty.audio.add('explosion', 'assets/explosion.wav');
        
        
        // start the game
        Crafty.scene('Game');
    },
    
    // Progress callback
    function(e){
        Crafty("LoadingScreen").showProgress(e);
    },
    
    // Error callback
    function(e){
        alert("error, assets not successfuly loaded");
    })
}) // ------------------------------------------------------------------------//

Crafty.scene('Pause', function()
{
    this.bind('KeyDown', function(e)
    {
       if(e.keycode == Crafty.keys['ENTER']) Crafty.scene('Game'); 
    });
}) // ------------------------------------------------------------------------//



