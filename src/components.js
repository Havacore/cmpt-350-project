// Everything object in the game
Crafty.c('Actor',{
    init : function()
    {
        this.requires('2D, Canvas');
    }, // ------------------------------------------------------------------- //
    
    at : function(x, y)
    {
        this.attr(
        {
            x: x,
            y: y
        })
        
    },
    
    getX : function()
    {
        
        return this.x;
        
    },
    
    getY : function()
    {
        return this.y;
    },
    
    getWidth : function()
    {
        return this.w;
    },
    
    getHeight : function()
    {
        return this.h;
    }
    
}); // ------------------------------------------------------------------- //    

// Player character
Crafty.c('PC', {
    
    
    init : function()
    {
        this.requires('Actor,Fourway, Collision, spr_pc, Keyboard')
        .fourway(4)
        .stopOnSolids()
        .crashIntoEnemies();
        this.attr(
        {
            x: 200 - 32,
            y: 500,
            w: 64, 
            h: 64,
            rotation: -90
        })
        
        this.life = 3;
        this.alive = true;
        this.maxBullets = 5;
        this.currentBullets = 0;
        
        this.bind('KeyDown', function(e)
        {
             if(e.key == Crafty.keys['SPACE']) 
                this.shoot((this.x + this.h/2), this.y - this.h);
        })
        
        this.bind('EnterFrame', function(e)
        {
            if(this.y > Game.height() + 20)
            {
                this.y -= this._movement.y;        
            }
        })
    }, // ------------------------------------------------------------------- //
    
    // take damage when colliding into an enemy
    crashIntoEnemies : function()
    {
        this.onHit('Enemy1', function(e)
        {
            var x;
            var y;
            var w;
            var h;
            
            for(var i = 0; i < e.length; i++)
            {
                e[i].obj.die();
                x = e[i].obj.getX();
                y = e[i].obj.getY();
                w = e[i].obj.getWidth();
                h = e[i].obj.getHeight();
            }
            this.life -= 1;
            
            if(this.life <= 0)
            {
                this.gameOver();
            }
            
            Crafty.e('Explosion')
            .at(x - w, y);
            
            Crafty.audio.play('explosion', 1);
        })
    },
    
    // Changes the game scene to a gameOver state, giving the player their
    // final score
    gameOver : function()
    {
        setScore(Crafty('ScoreBoard').score);
        Crafty.scene('GameOver');
        
    },
    
    // Registers a stop-movement function to be called when this entity
    // hits an entity with the solid component
    stopOnSolids : function()
    {
        this.onHit('Solid', this.stopMovement);
        
        return this;
    },
    
    // Stops movement
    stopMovement : function()
    {
        this._speed = 0;
        if (this._movement)
        {
            this.x -= this._movement.x;
            this.y -= this._movement.y;
        }
    },
    
    shoot : function(x, y)
    {
        if(this.currentBullets <= this.maxBullets)
        {
            Crafty.e('Bullet').at(x,y);
            this.currentBullets += 1;
        }
        
    },
    
    removeBullet : function(e)
    {
        e.destroy();
        this.currentBullets -= 1;
    }
    
    
}); // ------------------------------------------------------------------- //

Crafty.c('Bullet', {
    init: function()
    {
        this.requires('Actor, Color, Collision');
        
        this.live = true;
        
        this.attr({
            w: 4,
            h: 4
        })
        
        this.color('rgb(255, 0, 0)'),
        
        this.bind("EnterFrame", function(e)
        {
            if(this.live)
            {
                this.y -= 5;
            }
        })
        
        this.onHit("Enemy1", function(e)
        {
            this.live = false;
            
            var x;
            var y;
            var w;
            var h;
            
            for(var i = 0; i < e.length; i++)
            {
                
                e[i].obj.die();
                x = e[i].obj.getX();
                y = e[i].obj.getY();
                w = e[i].obj.getWidth();
                h = e[i].obj.getHeight();
            }
            Crafty.e('Explosion')
            .at(x - w, y);
            
            Crafty.audio.play('explosion', 1);
            Crafty("PC").removeBullet(this);
            Crafty("ScoreBoard").scorePoint();
        })
        
        this.onHit('Wall_Top', function(e)
        {
            Crafty("PC").removeBullet(this);
        })
    }
}); // ------------------------------------------------------------------- //

// Enemy
Crafty.c('Enemy1', {
    init : function()
    {
        this.requires('Actor, Collision, spr_enemy');
        
        this.alive = true;
        
        this.attr(
        {
            w: 32,
            h: 32,
            rotation: 90,
        })
        
        this.bind("EnterFrame", function (e)
        {
            if(this.alive)
            {
                this.y += 2;
            }
            
        })
        
        this.onHit("Bottom", function(e)
        {
            
            this.die();
        })
        
        
    },
    
    
    
    die : function()
    {
        
        
        this.alive = false;
        killEnemy(this);
    },
    
    spawn : function(x, y)
    {
      
        this.alive = true;
        this.attr({
            x: x,
            y: y
        });
        
    },
    
    
    
    
}); // ------------------------------------------------------------------- //

Crafty.c('Explosion', {
    init : function()
    {
        this.requires('Actor, SpriteAnimation, spr_explosion')
        .animate('boom1', 0, 0, 63)
        .attr({
            w: 64,
            h: 64
        });
        
        this.animationSpeed = 1;
        this.numFrames = 64;
        this.lifespan = this.animationSpeed * this.numFrames;
        this.life = 0;
        
        this.animate('boom1', this.animationSpeed, 1);
        
        this.bind('EnterFrame', function(e)
        {   
            if(++this.life > this.lifespan )
            {
                this.destroy();
            }
        })
    },
    
    
    
}); // ------------------------------------------------------------------- //

Crafty.c('ScoreBoard', {
    init: function()
    {
        this.requires('Text, Actor, DOM');
        this.attr({
            x: 0,
            y: 10,
            w: Game.width(),
            h: 20
        })
        
        this.text("Score: 0");
        this.score = 0;
    },
    
    scorePoint : function()
    {
        
        this.score++;
        this.text("Score: " + this.score)
    }
}); // ------------------------------------------------------------------- //

Crafty.c('LoadingScreen', {
    init : function()
    {
        this.requires('Text, Actor, DOM');
        this.attr({
            x: 0,
            y: 10,
            w: Game.width(),
            h: 20
        })
        
        this.text("Loading...");
    },
    
    showProgress : function(e)
    {
        this.text("Loading...  " + e);
    }
});  // ------------------------------------------------------------------- //

Crafty.c('GameOver_Screen', {
    
    init: function()
    {
        this.requires('Text, Actor, DOM');
        this.attr({
            x: 0,
            y: 10,
            w: Game.width(),
            h: 20
        })
        
        this.score = 0;
        
        this.text("Game Over\n Score: ");
    },
    
    setScore : function(score)
    {
        this.score = score;
        this.text("Game Over\n Score: " + this.score);
    }
    
});   // ------------------------------------------------------------------- //

Crafty.c('Wall_Left', {
    init: function()
    {
        this.requires('Actor, Solid');
        this.attr({
            x: -20,
            y: 0,
            w: 1,
            h: Game.height()
        })
    }
});

Crafty.c('Wall_Right', {
    init: function()
    {
        this.requires('Actor, Solid');
        this.attr({
            x: Game.width() + 20,
            y: 0,
            w: 2,
            h: Game.height()
        })
    }
} )

Crafty.c('Wall_Top', {
    init: function()
    {
        this.requires('Actor, Solid');
        this.attr({
            x: 0,
            y: 0,
            w: Game.width(),
            h: 2 // height needed to be more than 1 to register contacts accurately
        })
    }
} )

Crafty.c('Wall_Bottom', {
    init: function()
    {
        this.requires('2D, Solid, Bottom');
        this.attr({
            x: 0,
            y: Game.height(),
            w: Game.width(),
            h: 2
        })
        
    }
} ) // ------------------------------------------------------------------- //

















